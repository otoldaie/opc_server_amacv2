/* © Copyright CERN, Universidad de Oviedo, 2015.  All rights not expressly granted are reserved.
 * QuasarServer.cpp
 *
 *  Created on: Nov 6, 2015
 * 		Author: Damian Abalo Miron <damian.abalo@cern.ch>
 *      Author: Piotr Nikiel <piotr@nikiel.info>
 *
 *  This file is part of Quasar.
 *
 *  Quasar is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public Licence as published by
 *  the Free Software Foundation, either version 3 of the Licence.
 *
 *  Quasar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public Licence for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Quasar.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <Configuration.hxx>
#include <thread>
#include <ASAMAC.h>
#include <Base_DAMAC.h>

#include "QuasarServer.h"
#include <LogIt.h>
#include <shutdown.h>
#include <DRoot.h>
#include <DAMAC.h>
#include <DFibreA.h>
#include <DSubSystem.h>

//#include </home/grosin/Desktop/itsdaq-sw/stlib/amac2.h>

QuasarServer::QuasarServer() : BaseQuasarServer()
{
  
}

QuasarServer::~QuasarServer()
{
  
}

void QuasarServer::mainLoop()
{
  printServerMsg("Press "+std::string(SHUTDOWN_SEQUENCE)+" to shutdown server");
  
  // Wait for user command to terminate the server thread.

  // run multi- or single-threaded
  bool run_multithreaded = false;

  // So, I want config parameters for the OPC server operation itself.
  // But I cannot define config parameters in the root node of the server.
  // What should be done:
  // have one top level node right under root to configure the server logic.
  // Right now:
  // multiple SubSystem top nodes are allowed.
  // Let's limit them to 1, and define the server params there.
  // If needed, let's add one more top node in the future. It will change the datapoints in WinCC
  const std::vector<Device::DSubSystem* >& subsystems = Device::DRoot::getInstance()->subsystems();

  if (subsystems.size() < 1) {
    printServerMsg("No subsystems in the config! Shutting down server"); return;
  }

  if (subsystems[0] == nullptr) {
    printServerMsg("The subsystem 0 is nullptr? Shutting down server"); return;
  }

  run_multithreaded = subsystems[0]-> RunMultithreaded();

  // if it runs multithreaded,
  // launch the threads per FLX fibre
  // "full.opc.name": thread
  std::map<std::string, std::thread*> fibre_ec_threads;

  if (run_multithreaded) {
    for (Device::DSubSystem *sys : Device::DRoot::getInstance()->subsystems())
      {
      for (Device::DFibreA *fibreA : sys->fibreas())
        {
        //amac->read_command();
        //fibreA->linkAMACs();
        //fibre_ec_threads[fibreA->getFullName()](fibreA->processAMACs);
        //fibre_ec_threads[fibreA->getFullName()](&Device::DFibreA::processAMACs, fibreA);
        fibre_ec_threads[fibreA->getFullName()] = new std::thread{&Device::DFibreA::processAMACs, fibreA};
        }
      }
  }

  // run loop
  while(ShutDownFlag() == 0)
    {
      std::this_thread::sleep_for(std::chrono::milliseconds(10));

      // check on thread stats??

      // the single-threaded operation
      if (!run_multithreaded) {
      for (Device::DSubSystem *sys : Device::DRoot::getInstance()->subsystems())
        {
        for (Device::DFibreA *fibreA : sys->fibreas())
          {
          //amac->read_command();
          fibreA->linkAMACs();
          }
        }
      }
    }

  // if it is the multi-threaded operation
  // shut down the threads
  if (run_multithreaded) {
    for (Device::DSubSystem *sys : Device::DRoot::getInstance()->subsystems())
      {
      for (Device::DFibreA *fibreA : sys->fibreas())
        {
        //amac->read_command();
        //fibreA->linkAMACs();

        fibreA->stop();
        // check if the process function in the thread has finished
        int count_retries = 0;
        while (fibreA->running_state != Device::DFibreA::DONE)
          {
          fibreA->stop();

          count_retries += 1;
          if (count_retries > 2000)
            { throw std::runtime_error("QuasarServer::mainLoop() cannot stop a fibre thread!"); }

          std::this_thread::sleep_for(std::chrono::microseconds(500));
          }

        // the thread process has finished, it's safe to join it
        fibre_ec_threads[fibreA->getFullName()]->join();
        delete fibre_ec_threads[fibreA->getFullName()];
        fibre_ec_threads[fibreA->getFullName()] = nullptr;
        }
      }
  }

  printServerMsg(" Shutting down server");
  
}

void QuasarServer::initialize()
{
  LOG(Log::INF) << "Initializing Quasar server.";
  
}

void QuasarServer::shutdown()
{
  LOG(Log::INF) << "Shutting down Quasar server.";
}

void QuasarServer::initializeLogIt()
{
  BaseQuasarServer::initializeLogIt();
  LOG(Log::INF) << "Logging initialized.";
}
