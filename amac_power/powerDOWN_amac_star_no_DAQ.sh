#!/bin/bash
slow_delay=0 # for sleep command in the slow power up

if [ -z "$1" ]
then
  echo "provide the AMAC ID of the module" 1>&2
  exit 1
fi

>&2 echo "amac id $1"

default_cfg_file=/opt/atlas/etc/cfg_endeavour.json

if [ -z "$2" ]
then
  cfg_file=$default_cfg_file
else
  cfg_file=$2
fi

>&2 echo "amac cfg $cfg_file"

# setid argument
if [ -z "$3" ]
then
  echo no 3rd argument - no SETID
else
  echo got a 3rd argument $3 - sending SETID
  endeavour -c $cfg_file -i $1 setid idpads $1
fi


# star power up from write_amac_ppb_newer
echo regs

echo enable ABCy1 LPM
echo 40   0x77773700 ; endeavour -c $cfg_file -i $1 write  40   0x77773700  # turn off ABCy1LPM
sleep $slow_delay

echo enable ABCy0 LPM
echo 40   0x77771700 ; endeavour -c $cfg_file -i $1 write  40   0x77771700  # LDO LPMy HCC
sleep $slow_delay

echo enable HCCyLPM
echo 40   0x77770700 ; endeavour -c $cfg_file -i $1 write  40   0x77770700  # LDO LPMx ABC 1
sleep $slow_delay

echo enable ABCx1 LPM
echo 40   0x77770300 ; endeavour -c $cfg_file -i $1 write  40   0x77770300  # LDO LPMx ABC 0
sleep $slow_delay

echo enable ABCx0 LPM
echo 40   0x77770100 ; endeavour -c $cfg_file -i $1 write  40   0x77770100  # LDO LPMx
sleep $slow_delay

echo enable HCCx LPM
echo 40   0x77770000 ; endeavour -c $cfg_file -i $1 write  40   0x77770100  # LDO LPMx
sleep $slow_delay

echo disable DCDC
echo 41   0x00000000 ; endeavour -c $cfg_file -i $1 write  41   0x00000000  # DCDC disabled
