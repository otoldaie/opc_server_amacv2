#!/bin/bash

# Usage:
# for i in `seq 0 13`; do   powerUP_amac_star.sh $i <connection config>.json set; done
# the third argument ("set" above) makes it issue SETID
#
# example connection config is in
# config/connectivity_cfg_sr1_flx04_dev0_link01.json
#
# see in the config, it needs any existing file on the computer to use as a lock before connecting to the bus of AMACs:
#     "lock_filename": "/opt/atlas/etc//amac_lockfiles/flx03/0/lock_link01",
#     "comment_on_lock_filename": "it can be any file on the computer, but it has to exist -- AMAC connection tries to get a lock on the file before doing anything else"

slow_delay=0 # for sleep command in the slow power up

if [ -z "$1" ]
then
  echo "provide the AMAC ID of the module" 1>&2
  exit 1
fi

>&2 echo "amac id $1"

default_cfg_file=/opt/atlas/etc/cfg_endeavour.json
default_daq_cfg_file=CERNSR1_stave_testing_setup.json

if [ -z "$2" ]
then
  cfg_file=$default_cfg_file
else
  cfg_file=$2
fi

>&2 echo "amac cfg $cfg_file"

# setid argument
if [ -z "$3" ]
then
  echo no 3rd argument - no SETID
else
  echo got a 3rd argument $3 - sending SETID
  endeavour -c $cfg_file -i $1 setid idpads $1
fi


# star power up from write_amac_ppb_newer
echo regs

echo 42   0x00000000 ; endeavour -c $cfg_file -i $1 write  42   0x00000000  # no interlock or warnings enabled
echo 44   0x00000001 ; endeavour -c $cfg_file -i $1 write  44   0x00000001  # AM enabled
echo 45   0x00000100 ; endeavour -c $cfg_file -i $1 write  45   0x00000100  # require DCDC PGOOD, but do not power from DCDC
echo 46   0x010d010a ; endeavour -c $cfg_file -i $1 write  46   0x010d010a  # BG tuning for one of amacs
echo 47   0x00000000 ; endeavour -c $cfg_file -i $1 write  47   0x00000000  # default
echo 48   0x00000000 ; endeavour -c $cfg_file -i $1 write  48   0x00000000  # shunt values at zero
echo 49   0x0000000D ; endeavour -c $cfg_file -i $1 write  49   0x0000000D  # DAC bias at default
echo 50   0x00000704 ; endeavour -c $cfg_file -i $1 write  50   0x00000704  # default AMACcnt, default is 0x404, max drive current 0x704
echo 51   0x03040404 ; endeavour -c $cfg_file -i $1 write  51   0x03040404  # default
echo 52   0x00401500 ; endeavour -c $cfg_file -i $1 write  52   0x00401500  # default

sleep $slow_delay
echo OFF
echo 41   0x00000000 ; endeavour -c $cfg_file -i $1 write  41   0x00000000  # DCDC disabled
echo 40   0x77770000 ; endeavour -c $cfg_file -i $1 write  40   0x77770000  # HV and LDOs are disabled -- low power mode
echo 43   0x00000000 ; endeavour -c $cfg_file -i $1 write  43   0x00000000  # HCC holding reset

sleep $slow_delay
echo DCDC
echo 41   0x00000001 ; endeavour -c $cfg_file -i $1 write  41   0x00000001  # DCDC enabled

sleep $slow_delay
echo HCCxLPM
echo 40   0x77770100 ; endeavour -c $cfg_file -i $1 write  40   0x77770100  # LDO LPMx

sleep $slow_delay
echo HCCxRESET
echo 43   0x00000100 ; endeavour -c $cfg_file -i $1 write  43   0x00000100  # HCC X reset

sleep $slow_delay
echo ABCx0LPM
echo 40   0x77770300 ; endeavour -c $cfg_file -i $1 write  40   0x77770300  # LDO LPMx ABC 0

sleep $slow_delay
echo disable ABCx1 LPM
echo 40   0x77770700 ; endeavour -c $cfg_file -i $1 write  40   0x77770700  # LDO LPMx ABC 1

sleep $slow_delay
echo LPM Y

echo disable HCCy LPM
echo 40   0x77771700 ; endeavour -c $cfg_file -i $1 write  40   0x77771700  # LDO LPMy HCC

sleep $slow_delay
echo HCCyRESET
echo 43   0x00010100 ; endeavour -c $cfg_file -i $1 write  43   0x00010100  # HCC Y reset

sleep $slow_delay
echo disable ABCy0 LPM
echo 40   0x77773700 ; endeavour -c $cfg_file -i $1 write  40   0x77773700  # LDO LPMy ABC 0

sleep $slow_delay
echo disable ABCy1 LPM
echo 40   0x77777700 ; endeavour -c $cfg_file -i $1 write  40   0x77777700  # LDO LPMy ABC 1
