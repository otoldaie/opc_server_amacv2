#!/bin/bash

python3 Cacophony/generateStuff.py \
    --dpt_prefix StaveSC --server_name STAVE_SC_FLX03 \
    --subscription STAVE_SC_FLX03_SUBSCRIPTION \
    --driver_number 250 --function_prefix staveSC

mv Cacophony/generated/configParser.ctl Cacophony/generated/configParser_allAMAC.ctl
mv Cacophony/generated/createDpts.ctl Cacophony/generated/createDpts_allAMAC.ctl
